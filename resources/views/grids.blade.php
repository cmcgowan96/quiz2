<!DOCTYPE html>
<html>
<head>
	<title></title>

	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>


	<div class="container-fluid">
		<div class="row" style="height: 100px;">
			<div class="col-sm-8">
				row 1 big box right
			</div>
			<div class="col-sm-4">
				row 1 small box left
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="row">
					<div class="col-sm-6">
						row 2 small box top left
					</div>
					<div class="col-sm-6">
						row 2 small box top right
					</div>					
				</div>
				<div class="row" style="height: 75px;">
					<div class="col-sm-6">
						row 2 small box bottom left
					</div>
					<div class="col-sm-6">
						row 2 small box bottom right
					</div>					
				</div>					
			</div>
			<div class="col-sm-8">
				row 2 big box right
			</div>
		</div>	
	</div>




	<script src="../../dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>